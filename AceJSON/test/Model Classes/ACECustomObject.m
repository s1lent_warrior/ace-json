//
//  ACEEnclosedObject.m
//  Objective-JSON
//
//  Created by Muneeb Ahmed Anwar on 06/07/2014.
//  Copyright (c) 2014 SoftAce. All rights reserved.
//

#import "ACECustomObject.h"

@implementation ACECustomObject

- (NSString *)description
{
    NSMutableString* desc = [NSMutableString stringWithFormat:@"CustomObject {\n"];
    [desc appendFormat:@"\tcustomObjectKey: %@\n", self.customObjectKey];
    [desc appendFormat:@"\tcustomObjectValue: %@\n", self.customObjectValue];
    [desc appendFormat:@"}"];
    return desc;
}

@end
