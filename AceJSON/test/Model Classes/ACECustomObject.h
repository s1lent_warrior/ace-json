//
//  ACEEnclosedObject.h
//  Objective-JSON
//
//  Created by Muneeb Ahmed Anwar on 06/07/2014.
//  Copyright (c) 2014 SoftAce. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ACECustomObject : NSObject

@property (nonatomic, strong) NSString* customObjectKey;
@property (nonatomic, strong) NSString* customObjectValue;

@end
