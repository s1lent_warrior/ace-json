//
//  ACECustomArrayObject.h
//  AceJSON
//
//  Created by Muneeb Ahmed Anwar on 07/07/2014.
//  Copyright (c) 2014 SoftAce. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ACECustomArrayObject : NSObject

@property (nonatomic, readwrite) int custArrObjId;
@property (nonatomic, strong) NSString* custArrObjName;

@end
