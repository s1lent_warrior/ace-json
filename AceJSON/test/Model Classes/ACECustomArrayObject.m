//
//  ACECustomArrayObject.m
//  AceJSON
//
//  Created by Muneeb Ahmed Anwar on 07/07/2014.
//  Copyright (c) 2014 SoftAce. All rights reserved.
//

#import "ACECustomArrayObject.h"

@implementation ACECustomArrayObject

- (NSString *)description
{
    NSMutableString* desc = [NSMutableString stringWithFormat:@"CustomArrayObject {\n"];
    [desc appendFormat:@"\tcustArrObjId: %d\n", self.custArrObjId];
    [desc appendFormat:@"\tcustArrObjName: %@\n", self.custArrObjName];
    [desc appendFormat:@"}"];
    return desc;
}

@end
