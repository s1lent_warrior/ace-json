//
//  ACETestJson.m
//  Objective-JSON
//
//  Created by Muneeb Ahmed Anwar on 06/07/2014.
//  Copyright (c) 2014 SoftAce. All rights reserved.
//

#import "ACETestJson.h"

@implementation ACETestJson

@synthesize anInt;

- (id)init
{
    self = [super init];
    if (self) {
        self.simpleArray = [NSArray new];
        self.customArray = [NSArray new];
    }
    return self;
}

- (void)setAnInt:(int)_Id
{
    anInt = _Id;
}

- (NSString *)description
{
    NSMutableString* desc = [NSMutableString stringWithFormat:@"ACETestJson {\n"];
    [desc appendFormat:@"\tanInt: %d\n", self.anInt];
    [desc appendFormat:@"\taString: %@\n", self.aString];
    [desc appendFormat:@"\taFloat: %.02f\n", self.aFloat];
    [desc appendFormat:@"\tsimpleArray: %@\n", self.simpleArray];
    [desc appendFormat:@"\tcustomArray: %@\n", self.customArray];
    [desc appendFormat:@"\t%@\n", self.customObject];
    [desc appendFormat:@"}"];
    return desc;
}

- (NSString *)debugDescription
{
    return [self description];
}

@end
