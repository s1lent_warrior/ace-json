//
//  ACETestJson.h
//  Objective-JSON
//
//  Created by Muneeb Ahmed Anwar on 06/07/2014.
//  Copyright (c) 2014 SoftAce. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ACECustomObject.h"

@interface ACETestJson : NSObject

@property(nonatomic, readonly) int anInt;
@property(nonatomic, strong) NSString* aString;
@property(nonatomic, readwrite) float aFloat;
@property(nonatomic, strong) NSArray* simpleArray;
@property(nonatomic, strong) NSArray* customArray;
@property(nonatomic, strong) ACECustomObject* customObject;

@end
