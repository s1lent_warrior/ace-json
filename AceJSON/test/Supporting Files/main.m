//
//  main.m
//  AceJSON
//
//  Created by Muneeb Ahmed Anwar on 07/07/2014.
//  Copyright (c) 2014 SoftAce. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ACEAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ACEAppDelegate class]));
    }
}
