//
//  ACEJson.m
//  Objective-JSON
//
//  Created by Muneeb Ahmed Anwar on 06/07/2014.
//  Copyright (c) 2014 SoftAce. All rights reserved.
//

#import "ACEJson.h"
#import "ACECustomArrayObject.h"
#import "ACETestJson.h"

@interface ACEJson ()
{
}
@end

@implementation ACEJson

+ (NSArray*) arrayOfType:(Class)clazz fromJsonArray:(NSArray*)array caseSensitive:(BOOL)caseSensitive collectionType:(NSDictionary*)dictTypes
{
    NSMutableArray* arrObjects = [NSMutableArray new];
    for (NSDictionary* dictJson in array) {
        id object = [ACEJson objectOfType:clazz fromJsonDictionary:dictJson caseSensitive:caseSensitive collectionsTypes:dictTypes];
        [arrObjects addObject:object];
    }
    return arrObjects;
}

+ (id) objectOfType:(Class)clazz fromJsonDictionary:(NSDictionary*)dictJson caseSensitive:(BOOL)caseSensitive collectionsTypes:(NSDictionary*)dictTypes
{
    if (!clazz) {
        NSException* exception = [NSException exceptionWithName:@"NSInvalidArgumentException"
                                                         reason:@"Type of object can not be Nil!"
                                                       userInfo:Nil];
        @throw exception;
    }
    if (!dictJson || [dictJson count] == 0) {
        NSException* exception = [NSException exceptionWithName:@"NSInvalidArgumentException"
                                                         reason:@"Dictionary of object can not be Nil or empty!"
                                                       userInfo:Nil];
        @throw exception;
    }
    
    id object = [clazz new];
    unsigned int outCount;
    objc_property_t *properties = class_copyPropertyList(clazz, &outCount);
    for (int i = 0; i < outCount; i++) {
        objc_property_t property = properties[i];
        NSString* propertyName = [NSString stringWithUTF8String:property_getName(property)];
        id propertyValue = [ACEJson valueForPropertyWithName:propertyName
                                              fromDictionary:dictJson
                                               caseSensitive:caseSensitive];
        
        if (isPrimitiveProperty(property)) {
            [object setValue:propertyValue forKey:propertyName];
        } else if (isArrayProperty(property)) {
            if (isSimpleArray(propertyValue)) {
                [object setValue:propertyValue forKey:propertyName];
            } else {
                Class aClass = NSClassFromString(dictTypes[propertyName]);
                if (aClass) {
                    NSArray* customArray = [ACEJson arrayOfType:aClass
                                                  fromJsonArray:propertyValue
                                                  caseSensitive:caseSensitive
                                                 collectionType:dictTypes];
                    [object setValue:customArray forKey:propertyName];
                }
            }
            
        } else if (isCustomObjectProperty(property)) {
            Class aClass = NSClassFromString(typeForProperty(property));
            NSDictionary* aDict = dictJson[propertyName];
            id customObject = [ACEJson objectOfType:aClass
                                 fromJsonDictionary:aDict
                                      caseSensitive:caseSensitive
                                     collectionsTypes:dictTypes];
            [object setValue:customObject forKey:propertyName];
        } else {
            [object setValue:Nil forKey:propertyName];
        }
    }
    
    free(properties);
    
    return object;
}

+ (id) valueForPropertyWithName:(NSString*)propertyName fromDictionary:(NSDictionary*)dict caseSensitive:(BOOL)caseSensitive
{
    if (caseSensitive) {
        for (NSString* key in [dict allKeys]) {
            if ([key caseInsensitiveCompare:propertyName] == NSOrderedSame) {
                return dict[key];
            }
        }
    } else {
        return dict[propertyName];
    }
    return Nil;
}

static BOOL isPrimitiveProperty(objc_property_t property)
{
    NSString* type = typeForProperty(property);
    if ([type isEqualToString:@"NSString"] || [type length] == 1) {
        return YES;
    }
    return NO;
}

static BOOL isCustomObjectProperty(objc_property_t property)
{
    NSString* type = typeForProperty(property);
    if ([type hasPrefix:@"NS"]) {
        return NO;
    }
    return YES;
}

static BOOL isArrayProperty(objc_property_t property)
{
    NSString* type = typeForProperty(property);
    if ([type isEqualToString:@"NSArray"] || [type isEqualToString:@"NSMutableArray"]) {
        return YES;
    }
    return NO;
}

static NSString* typeForProperty(objc_property_t property)
{
    NSString* type = [[NSString stringWithUTF8String: property_getAttributes(property)] componentsSeparatedByString: @","][0];
    if ([type length] > 2) {
        NSString* formattedType = [type substringFromIndex:3];
        return [formattedType substringToIndex:formattedType.length - 1];
    }
    return [type substringFromIndex:1];
}

static BOOL isSimpleArray(NSArray* array)
{
    if ([array[0] isKindOfClass:[NSDictionary class]]) {
        return NO;
    }
    return YES;
}

@end
