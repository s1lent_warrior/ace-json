//
//  ACEJson.h
//  Objective-JSON
//
//  Created by Muneeb Ahmed Anwar on 06/07/2014.
//  Copyright (c) 2014 SoftAce. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface ACEJson : NSObject

- (instancetype) init __attribute__((unavailable("init not available")));

+ (id) objectOfType:(Class)clazz fromJsonDictionary:(NSDictionary*)dict caseSensitive:(BOOL)caseSensitive collectionsTypes:(NSDictionary*)dictTypes;

+ (NSArray*) arrayOfType:(Class)clazz fromJsonArray:(NSArray*)array caseSensitive:(BOOL)caseSensitive collectionType:(NSDictionary*)dictTypes;

@end
